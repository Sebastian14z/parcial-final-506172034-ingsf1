<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: POST, DELETE, OPTIONS");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

ini_set('display_errors', 0);
ini_set('display_starup_error', 1);
error_reporting(E_ALL);
date_default_timezone_set('America/Bogota');

include($_SERVER['DOCUMENT_ROOT'] . '/parcial-final/libs/smarty/libs/Smarty.class.php');

class BaseController
{
    private $smarty;

    function __construct()
    {
        $this->smarty = new Smarty;
        $this->smarty->debugging = false;
        $this->smarty->caching = false;
        $this->smarty->template_dir = "views";
    }

    /**
     * @param $variables
     */
    protected function asignarVariableVista($variables)
    {
        foreach ($variables as $variable => $valor) {
            $this->smarty->assign($variable, $valor);
        }
    }

    /**
     * @param $vista
     * @throws SmartyException
     */
    protected function renderizarVista($vista)
    {
        $this->smarty->display($vista);
    }
}