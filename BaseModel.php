<?php
include($_SERVER['DOCUMENT_ROOT'] . '/parcial-final/conexion/conexion.php');

class BaseModel extends conexion
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $sql_str
     * @return mixed
     */
    public function consultar($sql_str)
    {
        $result_array = [];
        $results = $this->conexion->query($sql_str);
        foreach ($results as $key) {
            $result_array[] = $key;
        }
        return $this->convertirUTF8($result_array);
    }
}