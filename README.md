# PARCIAL-FINAL-506172034-INGSF1

Para el correcto funcionamiento del proyecto se deben tener en cuenta los siguientes aspectos:

# Servidor local:
Se debe tener instalado XAMPP en el equipo.

# Base de datos:
Se debe tener sin permisos la conexion del usuario "root" en MySQL.

# Localización del proyecto:
Se debe almacenar en el directorio de "htdocs" de la carpeta "xampp", generalmente se encuentra en la ruta default.

# Ruta:
http://localhost/parcial-final-506172034-ingsf1/index.php
