<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/parcial-final/BaseModel.php');

class Usuario
{
    private $cod_usuario, $nombres, $apellidos, $edad, $sexo, $identificacion;

    function __construct($usuario)
    {
        $this->cod_usuario = $usuario['cod_usuario'];
        $this->nombres = $usuario['nombres'];
        $this->apellidos = $usuario['apellidos'];
        $this->edad = $usuario['edad'];
        $this->sexo = $usuario['sexo'];
        $this->identificacion = $usuario['identificacion'];
    }

    public function getUsuario()
    {
        return [
            'cod_usuario' => $this->cod_usuario,
            'nombres' => $this->nombres,
            'apellidos' => $this->apellidos,
            'edad' => $this->edad,
            'sexo' => $this->sexo,
            'identificacion' => $this->identificacion
        ];
    }
}