<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: POST, DELETE, OPTIONS");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

ini_set('display_errors', 0);
ini_set('display_starup_error', 1);
error_reporting(E_ALL);
date_default_timezone_set('America/Bogota');

include_once($_SERVER['DOCUMENT_ROOT'] . '/parcial-final/BaseController.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/parcial-final/BaseModel.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/parcial-final/Usuario.php');

class UsuarioController extends BaseController
{
    private $base_model, $usuarios;

    /**
     * @throws SmartyException
     */
    function __construct()
    {
        parent::__construct();
        $this->base_model = new BaseModel();
        $this->usuarios = [];
        $this->instanciarUsuarios();
        $this->index();
    }

    /**
     * @throws SmartyException
     */
    private function index()
    {
        $usuarios_instanciados = [];
        foreach ($this->usuarios as $usuario) {
            $usuarios_instanciados[] = $usuario->getUsuario();
        }
        $this->asignarVariableVista(['usuarios' => $usuarios_instanciados]);
        $this->renderizarVista('usuarios.tpl');
    }

    private function instanciarUsuarios()
    {
        $sql = "select * from usuario";
        $consulta = $this->base_model->consultar($sql);
        foreach ($consulta as $usuario) {
            $this->usuarios[] = new Usuario($usuario);
        }
    }
}