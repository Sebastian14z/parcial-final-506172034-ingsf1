create schema parcialFinal;
use
parcialFinal;
create table usuario
(
    cod_usuario    int null,
    nombres        varchar(255) null,
    apellidos      varchar(255) null,
    edad           int null,
    sexo           varchar(255) null,
    identificacion varchar(255) null
);
INSERT INTO usuario (cod_usuario, nombres, apellidos, edad, sexo, identificacion)
VALUES (1, 'Nombre #1', 'Apellido #1', 10, 'Masculino', '10000');

INSERT INTO usuario (cod_usuario, nombres, apellidos, edad, sexo, identificacion)
VALUES (2, 'Nombre #2', 'Apellido #2', 11, 'Femenino', '10001');

INSERT INTO usuario (cod_usuario, nombres, apellidos, edad, sexo, identificacion)
VALUES (3, 'Nombre #3', 'Apellido #3', 12, 'Masculino', '10002');

INSERT INTO usuario (cod_usuario, nombres, apellidos, edad, sexo, identificacion)
VALUES (4, 'Nombre #4', 'Apellido #4', 13, 'Femenino', '10003');

INSERT INTO usuario (cod_usuario, nombres, apellidos, edad, sexo, identificacion)
VALUES (5, 'Nombre #5', 'Apellido #5', 14, 'Masculino', '10004');

INSERT INTO usuario (cod_usuario, nombres, apellidos, edad, sexo, identificacion)
VALUES (6, 'Nombre #6', 'Apellido #6', 15, 'Femenino', '10005');

INSERT INTO usuario (cod_usuario, nombres, apellidos, edad, sexo, identificacion)
VALUES (7, 'Nombre #7', 'Apellido #7', 16, 'Masculino', '10006');

INSERT INTO usuario (cod_usuario, nombres, apellidos, edad, sexo, identificacion)
VALUES (8, 'Nombre #8', 'Apellido #8', 17, 'Femenino', '10007');

INSERT INTO usuario (cod_usuario, nombres, apellidos, edad, sexo, identificacion)
VALUES (9, 'Nombre #9', 'Apellido #9', 18, 'Masculino', '10008');

INSERT INTO usuario (cod_usuario, nombres, apellidos, edad, sexo, identificacion)
VALUES (10, 'Nombre #10', 'Apellido #10', 19, 'Femenino', '10009');
