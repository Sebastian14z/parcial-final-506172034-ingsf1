<!doctype html>
<html lang="es">
<head>
    {* RESPONSIVE *}
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {* BOOTSTRAP 5 *}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>
    {* GOOGLE FONTS *}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300&display=swap" rel="stylesheet">
    <title>Repositorios</title>
    <style>
        body {
            padding: 0 !important;
            font-family: 'Nunito', sans-serif;
        }

        .card {
            background-color: black;
            color: white;
        }

        .col-10 {
            margin-top: 2%;
        }

        thead {
            background-color: #3333;
        }
    </style>
</head>
<body class="container-fluid">
<div class="row justify-content-md-center">

    <div class="card col-12">
        <div class="card-body">
            <h2 class="text-center">Fundación Universitaria Konrad Lorenz</h2>
            <h3 class="text-center">Ingenieria de Software I</h3>
            <h3 class="text-center">Parcial Final - MVC</h3>
            <h4 class="text-center">Johan Sebastian Masmela Gomez - 506172034</h4>
        </div>
    </div>

    <div class="col-10">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Código Usuario</th>
                <th scope="col">Nombres</th>
                <th scope="col">Apellidos</th>
                <th scope="col">Edad</th>
                <th scope="col">Sexo</th>
                <th scope="col">Identificacion</th>
            </tr>
            </thead>
            <tbody>
            {foreach item=usuario from=$usuarios}
                <tr>
                    <th>{$usuario.cod_usuario}</th>
                    <td>{$usuario.nombres}</td>
                    <td>{$usuario.apellidos}</td>
                    <td>{$usuario.edad}</td>
                    <td>{$usuario.sexo}</td>
                    <td>{$usuario.identificacion}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
</body>
</html>